﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchGameobjects : MonoBehaviour
{
    [SerializeField]
    public GameObject[] gameObjectsToSwitch;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoopActivation());
    }

    // Update is called once per frame
    void Update()
    {

    }

    int currentActivated = 0;
    void LoopBetween()
    {
        for (int i = 0; i < gameObjectsToSwitch.Length; i++)
        {
            if (i == currentActivated)
            {
               StopObject(gameObjectsToSwitch[i]);
            }
            else
            {
               TurnOnObject(gameObjectsToSwitch[i]);
                // gameObjectsToSwitch[i].SetActive(true);
            }
        }

        currentActivated++;

        if (currentActivated >= gameObjectsToSwitch.Length)
        {
            currentActivated = 0;
        }
    }

    [SerializeField]
    float timeToWait;
    IEnumerator LoopActivation()
    {
        LoopBetween();
        yield return new WaitForSeconds(timeToWait);
        StartCoroutine(LoopActivation());
    }

    void StopObject(GameObject gameObject)
    {
        // foreach (Transform child in gameObject.transform)
        // {
        //     if (child.GetComponent<ParticleSystem>())
        //     {
        //         child.GetComponent<ParticleSystem>().Stop();
        //     }
        //     else
        //     {
        //         child.gameObject.SetActive(false);
        //     }
        // }

        gameObject.SetActive(false);
        // gameObject.GetComponent<ObjectInfo>().particleSystem.Stop();
                gameObject.GetComponent<ObjectInfo>().particleSystem.enableEmission =  false;

    }

    void TurnOnObject(GameObject gameObject)
    {
        // foreach (Transform child in gameObject.transform)
        // {
        //     if (child.GetComponent<ParticleSystem>())
        //     {
        //         child.GetComponent<ParticleSystem>().Play();
        //     }
        //     else
        //     {
        //         child.gameObject.SetActive(true);
        //     }
        // }
        gameObject.SetActive(true);
        gameObject.GetComponent<ObjectInfo>().particleSystem.enableEmission =  true;

    }
}
